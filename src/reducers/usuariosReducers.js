import { TRAER_TODOS, CARGANDO, ERROR } from '../types/usuariosTypes';

const INITIAL_STATE = {
  usuarios: [],
  cargando: false,
  error: ''
};

//El estado por default que va a tener el estado es el INITIAL_STATE
export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case TRAER_TODOS:
      //Se va a destructurar el estado
      return { 
        ...state, 
        usuarios: action.payload,
        cargando: false, //Termina la carga por que ya trajo a los usuarios 
        error: ''
      };

    case CARGANDO:
    return { ...state, cargando: true /*Esta cargando por que esta trayendo a los usuarios*/ };

    case ERROR:
    return { ...state, error: action.payload, cargando: false /* Termina la carga por que hubo un error */ };

      default: return state;
  }
}
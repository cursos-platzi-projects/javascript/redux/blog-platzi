import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import './css/iconos.css';
import App from './components/App';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reduxThunk from 'redux-thunk';

import reducers from './reducers'

//Se necesita crear un almacenamiento, para crearlo es asi
const store = createStore(
	reducers, //Todos los reducers
	{}, //Estado inicial
	applyMiddleware(reduxThunk)
);

ReactDOM.render(
	//Cuando manejas redux siempre lo debes de encerrar con el provider y se le va a decir a que almancenamiento va a tener
	<Provider store = { store }>
		<App />
	</Provider>,
	document.getElementById('root')
);

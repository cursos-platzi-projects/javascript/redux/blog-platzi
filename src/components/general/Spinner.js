import React from 'react';
import '../../css/spinner.css';

/* No necesita estado ni ciclo de vida y es un componente "Stateless" */

const Spinner = (props) => (
  <div className="center">
    <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
  </div>
);

export default Spinner
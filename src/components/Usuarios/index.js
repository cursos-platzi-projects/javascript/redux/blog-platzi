import React, { Component } from 'react';
import { connect } from 'react-redux';
import Spinner from '../general/Spinner';
import Fatal from '../general/Fatal';
import Tabla from './Tabla';

// * as = Para importar todo, o si no te da un error de que no esta exportando, eso de importar todo te evitar el hacer todo esto: { ejemplo1, ejemplo2, ejemplo3, etc }
import * as usuariosActions from '../../actions/usuariosActions';

class Usuarios extends Component {

	componentDidMount() {
		//Condicion para evitar que se traiga de nuevo los usuarios, esto pasaba cuando cambiabas de pantalla o ibas a publicaciones y regresabas de nuevo
		if(!this.props.usuarios.length)
		this.props.traerTodos()
	}

	ponerContenido = () => {
		if(this.props.cargando) {
			return <Spinner />;
		}

		if(this.props.error) {
			return <Fatal mensaje={this.props.error} />;
		}

		return <Tabla />
	}

	

	render() {
		return (
			<div>
				<h1>Usuarios</h1>
				{ this.ponerContenido() }
			</div>
		)
	}
};

//mapStateToProps = para mapear todos los reducers y obtener el que se desea
const mapStateToProps = (reducers) => {
	return reducers.usuariosReducer;
};

// connect = Para conectar el almacenamiento con el componente
// Como segundo parametro se le envia todos los actions
// y con el mapStateToProps se lo esta pasando al componente
export default connect(mapStateToProps, usuariosActions)(Usuarios);
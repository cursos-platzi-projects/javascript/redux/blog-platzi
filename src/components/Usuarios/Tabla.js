import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';

/* El uso del "this" es solo para las clases, no para los componentes como este que no es una clase */

/* Quitamos los parentesis para que podamos poner las llaves {} y de ese modo poner un return
Esto fue el caso para que podamos poner funciones */
const Tabla = (props) => {
  const ponerFilas = () => props.usuarios.map((usuario, key) => (
		<tr key={ usuario.id }>
			<td>
				{ usuario.name }
			</td>
			<td>
				{ usuario.email }
			</td>
			<td>
				{ usuario.website }
			</td>
      <td>
        <Link to={ `/publicaciones/${key}` }>
          <div className="eye-solid icon"></div>
        </Link>
      </td>
		</tr>
  ));

  return (
    <div>
      <table className="tabla">
        <thead>
          <tr>
            <th>
              Nombre
            </th>
            <th>
              Correo
            </th>
            <th>
              Enlace
            </th>
          </tr>
        </thead>
        <tbody>
          { ponerFilas() }
        </tbody>
      </table>
    </div>
  )
};

const mapStateToProps = (reducers) => {
  return reducers.usuariosReducer;
}

export default connect(mapStateToProps)(Tabla);
// No es necesario obtener las acciones ya que ya estamos obteniendo a los usuarios
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Cargando from '../general/Spinner';
import Fatal from '../general/Fatal';

import * as usuariosActions from '../../actions/usuariosActions';
import * as publicacionesActions from '../../actions/publicacionesActions';
import Spinner from '../general/Spinner';

// Se hace esto para que podamos renombrarlos, por que los dos tienen el mismo nombre del metodo.
const { traerTodos: usuariosTraerTodos } = usuariosActions;
const { traerPorUsuario: publicacionesTraerPorUsuario, abrirCerrar } = publicacionesActions;

class Publicaciones extends Component {
  // Se le pone el asyn para que la funcion "usuariosTraerTodos" se espere a su resultado para asi despues comienze con el siguiente
  async componentDidMount() {
    const {
      usuariosTraerTodos,
      publicacionesTraerPorUsuario,
      match: { params: { key } }
    } = this.props

    if(!this.props.usuariosReducer.usuarios.length) {
      //await this.props.usuariosTraerTodos();
      await usuariosTraerTodos(); //Se corto codigo por la destructuracion de codigo
    }

    if(this.props.usuariosReducer.error) {
      return;
    }
    if(!('publicaciones_key' in this.props.usuariosReducer.usuarios[key])) {
      // Como parametro es el userId
      //this.props.publicacionesTraerPorUsuario(this.props.match.params.key);
      publicacionesTraerPorUsuario(key);
    }
    
  }

  ponerUsuario = () => {
    const { 
      usuariosReducer,
      match: { params: { key } } 
    } = this.props;

    if(usuariosReducer.error) {
      return <Fatal mensaje={ usuariosReducer.error }/>
    }

    if(!usuariosReducer.usuarios.length || usuariosReducer.cargando) {
      return <Spinner />
    }

    const nombre = usuariosReducer.usuarios[key].name;

    return (
        <h1>
          Publicaciones de {nombre}
        </h1>
    )
  };

  ponerPublicaciones = () => {
    //Se puede destructurar "usuariosReducer" por que no hace ningun cambio en el estado
    const {
      usuariosReducer,
      usuariosReducer: { usuarios },
      publicacionesReducer,
      publicacionesReducer: { publicaciones },
      match: { params: { key } }
    } = this.props

    if(!usuarios.length) return;
    if(usuariosReducer.error) return; //No se hace nada por que ya lo estamos haciendo desde ponerUsuario()

    if(publicacionesReducer.cargando) {
      return <Spinner />;
    }

    if(publicacionesReducer.error) {
      return <Fatal mensaje = {publicacionesReducer.error}/>
    }
    //No se hace nada con el resultado por que primero se espera a que carge los usuarios para despues las publicaciones
    if(!publicaciones.length) return;
    if(!('publicaciones_key' in usuarios[key])) return;

    const { publicaciones_key } = usuarios[key];

    return this.mostrarInfo(
      publicaciones[publicaciones_key],
      publicaciones_key
    );
  };

  mostrarInfo = (publicaciones, pub_key) => (
    publicaciones.map((publicacion, com_key) => (
      <div 
        className="pub_titulo"
        key = { publicacion.id }
        onClick = { () => this.props.abrirCerrar(pub_key, com_key) }
      >
        <h2>
          { publicacion.title }
        </h2>
        <h3>
          { publicacion.body }
        </h3>
      </div>
    ))
  );

  render() {
    console.log(this.props)
    return (
      <div>
        { this.ponerUsuario() }
        { this.ponerPublicaciones() }
      </div>
    )
  }
}

//Para aceptar multiples reducers
const mapStateToProps = ({ usuariosReducer, publicacionesReducer }) => {
  return {
    usuariosReducer,
    publicacionesReducer
  }
};

const mapDispatchToProps = {
  usuariosTraerTodos,
  publicacionesTraerPorUsuario,
  abrirCerrar
}

export default connect(mapStateToProps, mapDispatchToProps)(Publicaciones);

/*this.props.match.params.key = con esto me aparece el numero que aparece en la URL
Es como hacer un get para obtener los valores pasado por la URL */

/* NOTA: Aqui se estan implementado varios reducers, el codigo cambia un poco por el que solo es un solo reducer en el caso esta en el archivo: Usuarios => index.js */

/*
Esta condicion puede regresar un true
if('publicaciones_key' in this.props.usuariosReducer.usuarios[this.props.match.params.key])

Si quieres que sea lo contrario como una negacion, se puede encapsular en un parentesis y poner el signo de negacion

if(!('publicaciones_key' in this.props.usuariosReducer.usuarios[this.props.match.params.key]))
*/


/**
No se destructura esto: this.props.usuariosReducer.usuarios
Ya que como es el estado del reducer, y en el componentDidMount cambia solo una vez, en el estado son varias veces
Entender mejor video: "Evitar sobrescritura" minuto: "04:40"
 */
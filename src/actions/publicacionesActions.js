import axios from 'axios';
import { TRAER_POR_USUARIO, CARGANDO, ERROR } from '../types/publicacionesTypes';
import * as usuariosTypes from '../types/usuariosTypes';

const { TRAER_TODOS: USUARIOS_TRAER_TODOS } = usuariosTypes;

// getState = obtener el estado actual del reducer que le estas especificando
export const traerPorUsuario = (key) => async (dispatch, getState) => {

  dispatch({
    type: CARGANDO
  });

  const { usuarios } = getState().usuariosReducer; // Obtener los usuarios del reducer
  const { publicaciones } = getState().publicacionesReducer;
  const usuario_id = usuarios[key].id; //Con esto obtienes como el id del usuario

  try {
    const respuesta = await axios.get(`https://jsonplaceholder.typicode.com/posts?userId=${usuario_id}`);

    const nuevas = respuesta.data.map((publicacion) => ({
      ...publicacion,
      comentarios: [],
      abierto: false
    }));

    const publicaciones_actualizadas = [
      ...publicaciones,
      nuevas
      //respuesta.data
    ];

    dispatch({
      type: TRAER_POR_USUARIO,
      payload: publicaciones_actualizadas
    });

    //Con este codigo hacemos la inmutabilidad
    const publicaciones_key = publicaciones_actualizadas.length - 1;
    const usuarios_actualizados = [...usuarios];
    usuarios_actualizados[key] = {
      ...usuarios[key],
      publicaciones_key
    }

    // Nuevo dispatch para que mande los usuarios actualizados
    dispatch({
      type: USUARIOS_TRAER_TODOS,
      payload: usuarios_actualizados
    });
  } catch(error) {
    console.log(error.message);
    dispatch({
      type: ERROR,
      payload: 'Publicaciones no disponibles.'
    });
  }
}

export const abrirCerrar = (pub_key, com_key) => (dispatch) => {
  console.log(pub_key, com_key);
}


/* En las variables publicaciones_key
no es necesario dejarlo asi:
publicaciones_key: publicaciones_key
por que la nomenglatura es igual asi que solo lo puede dejar sin los dos puntos y solo con las variable publicaciones_key */



// Lo comentado solo fue un ejemplo
// Este codigo estaba antes del metodo "traerPorUsuario"
/*export const traerTodos = () => async (dispatch) => {
  try {
    const respuesta = await axios.get('https://jsonplaceholder.typicode.com/posts');
    dispatch({
      type: TRAER_TODOS,
      payload: respuesta.data
    })
  }
  catch (error) {
    console.log(`Error: ${error.message}`)
    dispatch({
      type: ERROR,
      payload: 'Algo salio mal, intente mas tarde.'
    })
  }
}*/ 
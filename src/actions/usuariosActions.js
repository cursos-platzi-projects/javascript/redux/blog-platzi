import axios from 'axios';
import { TRAER_TODOS, CARGANDO, ERROR } from '../types/usuariosTypes';

// Aqui son don funciones: () => () =>
// dispatch = es el que va a disparar la llamada y va a contactar con el reducer para que haga todo este cambio de estado
export const traerTodos = () => async (dispatch) => {
  dispatch({
    type: CARGANDO
  })
  try {
    const respuesta = await axios.get('https://jsonplaceholder.typicode.com/users');
    dispatch({
      type: TRAER_TODOS,
      payload: respuesta.data
      //payload: [1,2,3]
    })
  } catch (error) {
    console.log(`Error: ${error.message}`)
    dispatch({
      type: ERROR,
      payload: 'Informacion de usuarios no disponible.'
    })
  }
}